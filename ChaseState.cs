﻿using UnityEngine;
using System.Collections;

public class ChaseState : IEnemyState
    //correct 1
   
{
    
    
    private readonly StatePatternEnemy enemy;
    

    public ChaseState(StatePatternEnemy statePatternEnemy)
    {
        enemy = statePatternEnemy;

    }

    public void UpdateState()
    {
        Look();
        Chase();
    }

    public void OnTriggerEnter(Collider other)
    {

    }

    public void ToPatrolState()
    {

    }

    public void ToAlertState()
    {
        enemy.currentState = enemy.alertState;
    }

    public void ToChaseState()
    {

    }

    private void Look()
    {
        
        RaycastHit hit;
        Vector3 enemyToTarget = (enemy.chaseTarget.position + enemy.offset) - enemy.eyes.transform.position;
        if (Physics.Raycast(enemy.eyes.transform.position, enemyToTarget, out hit, enemy.sightRange) && hit.collider.CompareTag("Player"))
        {
            PlaySound();
            enemy.m_AudioSource.Play();
            enemy.m_Animator.SetBool("see", true);
            enemy.chaseTarget = hit.transform;
            
        }
        else
        {
            enemy.m_Animator.SetBool("see", false);
            enemy.currentPlayerPosition = hit.transform;
            enemy.lastPosition = enemy.currentPlayerPosition;
            enemy.VecPos = enemy.lastPosition.transform.position; //saved the last position as a vector 3
            GameObject newWaypoint = GameObject.Instantiate(enemy.prefab, enemy.VecPos, Quaternion.identity) as GameObject;

            for (int i = 0; i < enemy.wayPoints.Count; i++)
            {
                enemy.wayPoints.RemoveAt(i);
                i--;
            }

            enemy.wayPoints.Add(newWaypoint.transform);//center

            Vector3 position1 = new Vector3(Random.Range(-10, 10), 0, Random.Range(-10, 10)) + enemy.VecPos;
            Vector3 position2 = new Vector3(Random.Range(-10, 10), 0, Random.Range(-10, 10)) + enemy.VecPos;
            Vector3 position3 = new Vector3(Random.Range(-10, 10), 0, Random.Range(-10, 10)) + enemy.VecPos;
            GameObject newWaypoint1 = GameObject.Instantiate(enemy.prefab, position1, Quaternion.identity);
            GameObject newWaypoint2 = GameObject.Instantiate(enemy.prefab, position2, Quaternion.identity);
            GameObject newWaypoint3 = GameObject.Instantiate(enemy.prefab, position3, Quaternion.identity);

            enemy.wayPoints.Add(newWaypoint1.transform);
            enemy.wayPoints.Add(newWaypoint2.transform);
            enemy.wayPoints.Add(newWaypoint3.transform);

            ToAlertState();
            
        }

    }

    private void Chase()
    {

        enemy.meshRendererFlag.material.color = Color.red;
        
        enemy.navMeshAgent.destination = enemy.chaseTarget.position;
        enemy.navMeshAgent.Resume();

        
    }

    private void PlaySound()
    {
        enemy.m_AudioSource.Play();
    }


}