﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[RequireComponent (typeof (Animator))]

public class StatePatternEnemy : MonoBehaviour
{
    public float searchingTurnSpeed = 120f;
    public float searchingDuration = 4f;
    public float sightRange = 35f;
    public List<Transform> wayPoints;
    public Transform eyes;
    public Vector3 offset = new Vector3(0, .5f, 0);
    public AudioClip alert;
    public GameObject prefab;
    public MeshRenderer meshRendererFlag;


  [HideInInspector]
    public AudioSource m_AudioSource;
    [HideInInspector]
    public Animator m_Animator;
    [HideInInspector]
    public Vector3 VecPos;
    [HideInInspector]
    public Transform chaseTarget;
    [HideInInspector]
    public IEnemyState currentState;
    [HideInInspector]
    public ChaseState chaseState;
    [HideInInspector]
    public AlertState alertState;
    [HideInInspector]
    public PatrolState patrolState;
    [HideInInspector]
    public UnityEngine.AI.NavMeshAgent navMeshAgent;
    

    public Transform lastPosition;
    public Transform currentPlayerPosition;

    private void Awake()
    {
        chaseState = new ChaseState(this);
        alertState = new AlertState(this);
        patrolState = new PatrolState(this);
        m_Animator = GetComponent<Animator>();
        navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        m_AudioSource = GetComponent<AudioSource>();
        m_AudioSource.clip = alert;
    }

    // Use this for initialization
    void Start()
    {
        currentState = patrolState;
    }

    // Update is called once per frame
    void Update()
    {
        currentState.UpdateState();
    }

    private void OnTriggerEnter(Collider other)
    {
        currentState.OnTriggerEnter(other);
    }
}